<?php

/**
 * App Settings
 * setting up directory structure.
 * 
 * NOTE: 
 *       - if installed a subdirectory change the root_path
 *       - views reside in the public folder.
 *       - NOTE: you'll have too remove the namespaces... or fix them and tell me how
 * $root_dir   = '';
 * $app_path   = 'mvc';
 * $views_path = 'views';
 * $use_composer = TRUE|FALSE
 */
$root_path    = 'smvc';
$app_path     = 'app';
$views_path   = 'views';
$use_composer =  FALSE;
/**
 * Database Settings;
 */
$db_driver   = 'sqlsrv';
$db_host     = ''; // YOUR SERVER HERE
$db_name     = ''; // YOUR DB NAME HERE
$db_user     = ''; // YOUR DB USER HERE
$db_password = '';


/**
 * TimeZone
 */
$timezone = 'America/Toronto';

/**
 * Charset / Encoding
 */
$charset = 'utf-8';

/**
 * global constants paths
 */
define('SERVER_ROOT',      $_SERVER['DOCUMENT_ROOT']      . DIRECTORY_SEPARATOR);
define('ROOT_PATH',        SERVER_ROOT   . $root_path     . DIRECTORY_SEPARATOR);
define('PRIVATE_PATH',     ROOT_PATH     . 'private'      . DIRECTORY_SEPARATOR);
define('PUBLIC_PATH',      ROOT_PATH     . 'public'       . DIRECTORY_SEPARATOR);
define('INCLUDES_PATH',    PRIVATE_PATH  . 'includes'     . DIRECTORY_SEPARATOR);
define('CORE_PATH' ,       PRIVATE_PATH  . 'core'         . DIRECTORY_SEPARATOR);
/////////////////////////////////////////////////////////////////////////////////

/**
 * APP PATHS
 */
define('APP_PATH' ,        $app_path                     . DIRECTORY_SEPARATOR);
define('CONTROLLERS_PATH', APP_PATH      . 'controllers' . DIRECTORY_SEPARATOR);
define('SERVICES_PATH',    APP_PATH      . 'services'    . DIRECTORY_SEPARATOR);
define('MODELS_PATH',      APP_PATH      . 'models'      . DIRECTORY_SEPARATOR);
define('CLASSES_PATH',     APP_PATH      . 'classes'     . DIRECTORY_SEPARATOR);
define('VIEWS_PATH',       PUBLIC_PATH   . $views_path   . DIRECTORY_SEPARATOR);
/////////////////////////////////////////////////////////////////////////////////
  
/**
 * PUBLIC PATHS
 */
/////////////////////////////////////////////////////////////////////////////////




/**
 * Set default timezone to ours.
 * List of available timezones can be found on PHP site.
 * @see http://www.php.net/manual/en/timezones.php
 */
define('TIMEZONE',$timezone);

/**
* set the site's default charset;
*/
define('SITE_ENCODING',$charset);


/**
 * Database Globals
 * 
 * supported drivers are; sqlsrv,mysql,odbc and sqlite for now...
 * you may uncomment the DSN for a custom dsn however no guarantee that everything will work.
 */
define('DB_DRIVER',   $db_driver);
define('DB_HOST',     $db_host);
define('DB_SCHEMA',   $db_name);
define('DB_USER',     $db_user);
define('DB_PASSWORD', $db_password);
// ONLY UNCOMMENT THIS IF YOU'RE USING A CUSTOM DRIVER
//define('DB_DSN', '');

define('USING_COMPOSER',$use_composer);