<?php
// This is to change all our globals...
require_once('config.include.php');

  /**
   * Autoload our classes,services, models and classes.
   *
   * @param string $class
   * @return void
   * @see http://php.net/manual/en/function.spl-autoload-register.php
   */
  function bootstrap_module($class) {
    $path = CLASSES_PATH;
    if(preg_match('/^(Controller|Service)$/',$class)) {
      $path = CORE_PATH;
    } elseif (preg_match('/Controller$/',$class)) {
      $path = CONTROLLERS_PATH;
    } elseif (preg_match('/Service$/',$class)) {
      if(preg_match('/^core/',$class))
        $path = CORE_PATH ;
      else 
        $path = SERVICES_PATH;   
    } elseif ((preg_match('/(Model|Result)$/', $class))) {
      $path = MODELS_PATH;             
    } else {
      if(file_exists(CLASSES_PATH . $class . '.php')) $path = CLASSES_PATH;
    }  
    
    try {
     
      require_once(sprintf('%s%s.php',$path,$class));
    } catch (Exception $ex){      
      exit($ex->getMessage());
    }
  }
  
  if (!USING_COMPOSER) 
    spl_autoload_register('bootstrap_module');
  else
    require(ROOT_PATH .  '/vendor/autoload.php');
  
  // RouteManger, must be loaded after autoloader
  require_once(CORE_PATH . 'Routes.php');