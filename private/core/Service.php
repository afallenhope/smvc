<?php
namespace MVC\Core\Services;
/**
 * Undocumented class
 * @abstract
 */
abstract class Service {
  protected $ch;

  // assign default options.
  public static options = array(
    'method'        => 'GET',    
    'headers'       => (array('User-Agent: StpidMVC/1.0')),
    'user_agent'    => $_SERVER['HTTP_USER_AGENT'],
    'timeout'       => 45,
    'show_progress' => FALSE,
    'onprogress'    => NULL,
    'oncomplete'    => NULL,
    'buffer_size'   => 1024,
    'return'        => TRUE,
  );

  /**
   * DELETE request method
   *    
   * @example Service::delete('/api',array('uid'=>1))
   *
   * @param string $url
   * @param array $params
   * @param closure $callback
   * @return void
   */
  public static function delete($url, $params = array(), $callback = NULL) {
    self::$options['method'] = 'DELETE';
    self::fetch($url,$params,$callback); 
  }
  
  /**
   * GET request method
   * @example Service::get('/api', array('uid') => 2)) 
   * @param string $url
   * @param closure $callback
   * @return void
   */
  public static function get($url, $params = array(), $callback = NULL) {

  }
  
  public static function post($url, $param = array(), $callback = NULL) {
    self::$options['method'] = 'POST';
    self::fetch($url,$params,$callback);  
  }

  public static function put ($url, $params = array(), $callback = NULL) {
    self::$options['method'] = 'PUT';
    self::fetch($url,$params,$callback);
  }
  
  /**
   * Fetch a page using cURL
   *
   * @param string $url
   * @param array $params
   * @param closure $callback
   * @return void
   */
  public static function fetch($url, $params = array(), $callback = NULL) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_URL ,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,self::$options['return']);

    // handle the request types.
    if (strtoupper(self::$options['method']) === 'PUT') {
      curl_setopt($ch, CURL_CUSTOMREQUEST,'PUT');
      self::$options['headers'][] = 'X-HTTP-Method-Override: PUT';
    } elseif (strtoupper(self::$options['method']) === 'DELETE') {
      curl_setopt($ch, CURL_CUSTOMREQUEST,'DELETE');
      self::$options['headers'][] = 'X-HTTP-Method-Override: DELETE';
    } elseif (strtoupper(self::$options['method']) === 'POST') curl_setopt($ch,CURLOPT_POST,1);
  
    if (count($params) >0) curl_setopt($ch,CURLOPT_POSTFIELDS,$params);

    if (self::$options['headers']) curl_setopt($ch,CURLOPT_HTTPHEADER,self::$options['headers']);    

    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, self::$options['timeout']);    
    curl_setopt($ch,CURLOPT_BUFFERSIZE,self::$options['buffer_size']);    
    $page = curl_exec($ch);

    $sr = new ServiceResult();
    if(curl_errno($ch)) {
      $sr['success'] = FALSE;
      $sr['error_no'] = curl_errno($ch);
      $sr['error_description'] = curl_error($ch);
      $sr['curl_request'] = $ch;
      $sr['options'] = self::$options;
      $sr['url'] = $url;
      $sr['params'] = $params;
    } else {
      $sr['success'] = TRUE;
      $sr['page'] = $page;
      $sr['curl_request'] = $ch;
      $sr['options'] = self::$options;
      $sr['url'] = $url;
      $sr['params'] = $params;      
      curl_close($ch);
    }

    if($callback) call_user_func_array($callback,$page);
    return $sr;
  }
}
?>