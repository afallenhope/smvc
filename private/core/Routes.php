<?php

RouteManager::hook('about', function(){
  $p = func_get_args();
   // should load before main content.
  ViewManager::CreateView('about/index',$p);
});

RouteManager::hook('contact', function(){
  $p = func_get_args();
  // No controller exists so it should redirect to erro however, we're pre-catching it first.
   echo 'contact/index';
});

RouteManager::hook('main', function(){
  $p = (func_num_args()>0) ? func_get_args() : array(); 
  $controller = (count($p) > 0) ? array_shift($p) : array();
  // should load normally
 ViewManager::CreateView('main/index',$controller); 
});