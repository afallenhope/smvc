<?php
/**
 * Base Controller Class
 * class MyController extends Controller
 * @abstract
 * @method void process()
 * @method void renderView()
 * @method void redorect($url)
 */
abstract class Controller {
  protected $data = array();
  protected $view = '';
  protected $head = array('title' => '', 'description' => '');

  /**
   * Process the controller
   * @example Controller->process() 'ControllerActivate' function.
   * @abstract
   * @param array $params
   * @return void
   */
  abstract function process($params);

  /**
   * Render a view via a controller (you can also use a view manager)
   * @example Controller->renderView() renders a view.
   *
   * @return void
   */
  public function renderView() {
    if ($this->view) {
      extract($this->data);
      $req_path = sprintf('%s%s.phtml',VIEWS_PATH , $this->view);
      $safe_path = filter_var($req_path,FILTER_SANITIZE_URL, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_BACKTICK);
      if(file_exists($safe_path)) include_once($safe_path);
    }
  }

  /**
   * Redirect to another resource path
   * @example Controller->redirect('http://mysite.com') redirects user
   *
   * @param string $url
   * @return void
   */
  public function redirect($url) {
    header('Location: ' . $url);
    header('Connection: close');
    exit;
  }
}