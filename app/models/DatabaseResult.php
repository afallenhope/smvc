<?php
class DatabaseResult {
 public $success;
 public $type;
 public $message;
 public $stacktrace;
 public $query;
 public $params;
}