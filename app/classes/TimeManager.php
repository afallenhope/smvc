<?php

class TimeManager {
  
  public static function setTZ($time) {
    date_default_timezone_set($time);
  }

  public static function getTZ() {
    return date_default_timezone_get();
  }
}