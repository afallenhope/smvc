<?php 
abstract class ViewManager {

  /**
   * CreateView
   *
   * @param string $viewName
   * @param [array] $params
   * @return void
   */
  public static function CreateView($viewName){
    if (is_null($viewName) || empty($viewName)) return;
    $view_path = sprintf('%s/%s.phtml',VIEWS_PATH,$viewName);
    $safe_view = filter_var($view_path,FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_BACKTICK);    
    
    $params = func_get_args();
    if(is_array($params) && count($params)>0){
      if(is_object($params[1])) $controller =$params[1];
      elseif (is_array($params[1])) $controller = array_shift($params[1]);
      else $controller = NULL;
    } 
    
    if($controller) {
      if( is_object($controller)) {
        if (property_exists($controller,'PRERENDER')) {
          include_once($safe_view);
        }       
      }
    }    
  }
}