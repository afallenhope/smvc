<?php

/**
 * Database class
 * @method PDOObject connect()
 * @method PDOStatement query($sql,[$params=array()])
 */
class Database {
  private static $instance = NULL;
  private static $query = NULL;
  
  /**
   * Connecto to Database
   * @example Database::connect()
   * 
   * @return PDOObject
   */
  private static function connect() {
    if(DB_DRIVER === 'mysql') {
      $dsn = spritnf('mysql:host=%s;dbname=%s;charset=%s',DB_HOST,DB_SCHEMA,SITE_ENCODING);
    } elseif (DB_DRIVER ==='sqlsrv') {
      $dsn = spritnf('sqlsrv:Server=%s;Database=%s;charset=%s',DB_HOST,DB_SCHEMA,SITE_ENCODING);
    }elseif (DB_DRIVER === 'sqlite') {
      $dsn = spritnf('sqlite:%s',DB_SCHEMA,SITE_ENCODING);
    } else {
      $dsn = DB_DSN;
    }

    try {
      self::$connection = new PDO($dsn,DB_USER,DB_PASSWORD);
      self::$connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
      return self::$connection
    } catch (PDOException $ex) {
      self::$connection = NULL;
      return $ex;
    }
    return self::$connection;

  }
  /**
   * query database
   * @example Database::query('SELECT * FROM table')
   *
   * @param string $query
   * @param array $params
   * @return Result
   */
  public static function query($query,$params = array()) {
    try {
      $stmt = self::$instance->prepare($query);
      if($stmt->execute($params)) {
        $result['success'] = TRUE;
        $result['message'] =  $stmt;
      } else {
        $result['success'] = FALSE;
        $result['type'] = 'query';
        $result['message'] = $stmt;
      }
    } catch (PDOException $ex) {
      $result['success'] = FALSE;
      $result['type'] = 'exception';
      $result['message'] = $ex->getMessage();
      $result['stacktrace'] = $ex;
    } finally {
      $result['query'] = $query;
      $result['params'] = $params;
      return $result;
    }
  }  
}

public static function where($condition) {
  if (!isset(self::$query) ||  is_null(self::$query)) throw new Exception('Unable to call \'where\' on non-object');
}