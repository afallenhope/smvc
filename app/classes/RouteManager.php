<?php
  /**
   * Route Manager
   * this can be used to hook routes..
   * 
   * @method void add($route,$callback)
   * @method boolean isValid($route)
   * @method void proceed($route,$params)
   */
  class RouteManager {
    protected static $hooks = array();

    /**
     * Add to our valid routes array
     * @example RouteManager::add('home',function($params){ print_r($params); });
     * 
     * @param string $route
     * @param closure $callback
     * @return void
     */
    public static function hook($route,$callback) {
      self::$hooks[$route] = $callback;
    }

    /**
     * Check to verify a valid route.
     * @example boolean RouteManager::isValid('home')
     * 
     * @param string $route
     * @return boolean
     */
    public static function hasHook($route) {
      return !empty(self::$hooks[$route]);
    }

    /**
     * Proceeds with the callback.
     * Called when a view matches.
     * @example void RouteManager::proceed('home');
     * 
     * @param closure $callback
     * @param array $params
     * @return void
     */
    public static function proceed($route,$params =array()) {      
      // array_shift($params);
      call_user_func(self::$hooks[$route],$params);
    }

    /**
     * Stops routing.
     *
     * @param string $route
     * @return void
     */
    public static function halt($route) {
      if(! headers_sent()) {
        header('HTTP/1.1 403 Unauthorized',TRUE,403);
        exit;
      }
    }
  }