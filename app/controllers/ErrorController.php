<?php
class ErrorController extends Controller {
  function process($params) {
    
    header("HTTP/1.1 404 Not Found",FALSE,404);
    $this->head['title'] = 'Error 404';
    $this->data = array('err_no'  => 404,'err_str' => 'Page Not Found', 'err_desc' =>'The resource you\'re looking for has moved or never existed.<br>This is not the page you are looking for.');
    $this->view = 'error/index';
  }
}