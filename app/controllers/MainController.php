<?php
  class MainController extends Controller { 
    public function process($params) { 
      $this->head = array('title' => 'Main Controller', 'description' => 'SMVC is a (M)odel (V)iew (C)ontroller platform with a simple hooking system.');
      $this->data = array('header'=>'Main Page', 'lead' =>'Welcome to SMVC.', 'content' =>  'SMVC is a (<strong>M</strong>)odel (<strong>V</strong>)iew (<strong>C</strong>)ontroller platform with a simple hooking system.'); 
      $this->view = 'main/index';
    }
  }