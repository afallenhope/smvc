<?php

/**
 * RouteController
 * 
 */
class RouteController extends Controller {
  protected $controller;

  /**
   * @method void process($params,[$isSubDir],[$subDir])
   *
   * @param [type] $params
   * @param boolean $isSubDir
   * @param string $subDir
   * @return void
   */
  public function process($params, $isSubDir=TRUE,$subDir='/wip') {
    $paths = $this->parseUrl(($isSubDir) ? str_replace($subDir,'', $params[0]) : $params[0]);

    if(empty($paths[0])) $this->redirect(($isSubDir) ? $subDir . '/main' : '/main');
    
    $route = array_shift($paths);
    $controller = $this->symbolToCamel($route) . 'Controller';
    try {
      if (file_exists(CONTROLLERS_PATH . $controller . '.php')) {
        $this->controller = new $controller;
        $this->controller->process($paths);
        $this->data['title'] = $this->controller->head['title'];
        $this->data['description'] = $this->controller->head['description'];
        // $this->data = (array)array_merge((array)$this->data , (array) $this->controller->data);
        $this->view = 'layout'; 
        $this->route = $route;
        $this->paths = $paths;
      } elseif(RouteManager::hasHook($route)) {
        RouteManager::proceed($route);
      }else {
        $this->redirect(($isSubDir) ? $subDir . '/error' : '/error');
      }
    } catch (Exception $ex) {
      $this->redirect(($isSubDir) ? $subDir . '/error' : '/error');
    }
  }
  

  /**
   * @method array parseUrl()
   *
   * @param string $url
   * @return void
   */
  private function parseUrl($url) {
    $parsed_url         = parse_url($url);
    $parsed_url['path'] = ltrim($parsed_url['path'],'/');
    $parsed_url['path'] = trim($parsed_url['path']);
    $exploded_url       = explode('/', $parsed_url['path']);
    return $exploded_url;
  }

  /**
   * any symbol in the URL will be converted to a space
   *
   * @param string $url
   * @return void
   */
  private function symbolToCamel($url) {
    $url = preg_replace('/[^0-9a-zA-Z ]/',' ',$url);
    if (is_array($url)) implode(' ', $url);
    $url = ucwords($url);
    $url = str_replace(' ','',$url);
    return $url;
  }

}