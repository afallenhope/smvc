<?php
// exit(basename(__DIR__));
// bootstrap wrapper
require_once('./private/includes/bootstrap.include.php');

// set internal encoding to ut8
// this forces standard encoding.
mb_internal_encoding(SITE_ENCODING);

// change the default timezone.
TimeManager::setTZ(TIMEZONE);

// Import our routes
require_once(CORE_PATH . 'Routes.php');

// instantiate our router
$routeController = new RouteController();
$safe_uri = filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL,FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_BACKTICK );
$routeController->process(array($safe_uri));
$routeController->renderView();
?>